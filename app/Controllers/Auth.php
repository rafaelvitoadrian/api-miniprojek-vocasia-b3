<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\User_model;
use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;

class Auth extends BaseController
{
    protected $user_model;
    use ResponseTrait;

    public function __construct()
    {
        $this->user_model = new User_model();
    }

    public function index()
    {
        $data = $this->user_model->findAll();
        return $this->respond($data,201);
    }

    public function register()
    {
        $validation = \Config\Services::validation();
        $rules = [
            'email' => [
                'rules' => 'required|is_unique[users.email]',
                'errors' => [
                    'required' => 'Silahkan Masukan Email',
                    'is_unique' => 'Email Sudah terdaftar'
                ]
            ],
            'name' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Silahkan Masukan Nama',
                ]
            ],
            'password' => [
                'rules' => 'required|min_length[8]',
                'errors' => [
                    'required' => 'Silahkan Masukan Password',
                    'min_length' => 'Password terlalu pendek'
                ]
            ],
            'password_confirmation' => [
                'rules' => 'required|matches[password]',
                'errors' => [
                    'required' => 'Silahkan Masukan Email',
                    'matches' => 'Password tidak sesuai'
                ]
            ]
        ];
        
        $validation->setRules($rules);
        if (!$validation->withRequest($this->request)->run()){
            return $this->fail($validation->getErrors());
        }

        $data_input = $this->request->getVar();
        $data = [
            'email' => $data_input['email'],
            'name' => $data_input['name'],
            'password' => md5($data_input['password']),
        ];
        
        $this->user_model->insert($data);
        $response = [
            'code' => 201,
            'error' => null,
            'message' => 'Berhasil Registrasi!'
        ];

       return $this->respond($response);
    }

    public function login()
    {
        $validation = \Config\Services::validation();
        $rules = [
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => 'isi email',
                    'valid_email' => 'silahkan masukan email yang valid'
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'isi password'
                ]
            ]
        ];

        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        }

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $ambil_email = $this->user_model->get_email($email);
        if ($ambil_email['password'] != md5($password)) {
            return $this->fail('Email Atau Password salah');
        }
        helper('jwt');
        $response = [
            'code' => 201,
            'error' => null,
            'data' => $ambil_email,
            'akses_token' => create_jwt($email)
        ];

        return $this->respond($response);

    }

}
