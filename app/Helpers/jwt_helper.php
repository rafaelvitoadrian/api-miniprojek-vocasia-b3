<?php

use App\Models\User_model;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

function get_jwt($otentikasi_header)
{
    if (!$otentikasi_header) {
        throw new Exception('Silahkan login terlebih dahulu');
    }
    return explode(' ', $otentikasi_header)[1];
}

function validate_jwt($encoded_token)
{
    $key = getenv('JWT_SECRET_KEY');
    $decoded_token = JWT::decode($encoded_token, new Key($key, 'HS256'));
    $user_model = new User_model();
    $user_model->get_email($decoded_token->email);
}

function create_jwt($email)
{
    $waktu_request = time();
    $waktu_token = getenv('JWT_TIME_TO_LIVE');
    $waktu_expired = $waktu_request + $waktu_token;
    $payload = [
        'email' => $email,
        'iap' => $waktu_request,
        'exp' => $waktu_expired
    ];
    $jwt = JWT::encode($payload, getenv('JWT_SECRET_KEY'), 'HS256');
    return $jwt;
}