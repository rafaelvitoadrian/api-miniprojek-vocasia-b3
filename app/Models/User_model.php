<?php

namespace App\Models;

use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Model;

class User_model extends Model
{
    use ResponseTrait;
    protected $DBGroup          = 'default';
    protected $table            = 'users';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'email','password','name'
    ];

    // Dates
    protected $useTimestamps = true;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];

    // public function get_email($email)
    // {
    //     $get_email = $this->db->table('users')->where('email', $email)->get()->getRow();
    //     if (!$get_email) {
    //         return $this->fail('Email Tidak Ditemukan');
    //     }
    //     return $get_email;
    // }

    public function get_all_users()
    {
        $data = $this->db->table('users')->get();
        return $data;
    }

    public function get_email($email)
    {
        $builder = $this->table('otentikasi');
        $data = $builder->where('email' , $email)->first();
        if (!$data)
        {
            throw new \Exception('Data Tidak Ditemukan');
        }
        return $data;
    }
}
